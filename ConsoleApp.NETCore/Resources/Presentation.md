# Visual Studio 2017 - New Features

[Visual Studio 2017 Feature Comparison](https://www.visualstudio.com/vs/compare/)

## IDE

### Performance
- Faster and componentized installation
    - The minimum installation is a few hundred MBs.
    - The minimum installation can open folders and supports over 20 programming languages. 
- Faster startup
- Faster solution load, by deferring or eliminating:
    -  Scanning installed components
    -  Initializing default settings
    -  Initializing caching
- Faster builds
- Less memory

### Navigation
- View > Go to ... (Ctrl + ,)
- Intellisense
  - Grouped by type
  - Capital typing...
  - Right click > Find all References
    - Colored
    - Grouping
    - Peek preview

### Debugging
- Run to Click
  - No need for temporary breakpoints anymore.
  - It includes perf glyph too.
- Live code analysis (Thanks to Roslyn)
- Real-time architecture validation (Thanks to Roslyn)
- Live unit testing (*THANKS TO ROSLYN!*)
  - Unfortunately only available in Enterprise edition.
    - Watch the [demo on Chanel 9](https://channel9.msdn.com/Events/Visual-Studio/Visual-Studio-2017-Launch/T105).
    - [Live Unit Testing in Visual Studio 2017 Enterprise](https://blogs.msdn.microsoft.com/visualstudio/2017/03/09/live-unit-testing-in-visual-studio-2017-enterprise/) on MSDN

## .NET Core
- Built-in tooling for VS 2017 (for the first time)
- Demo: .NET Core Console application
  - No project.json, but a simplified .csproj file!
  - Editable in Visual Studio!
  - It maintains the list of excluded files, not included (Try dropping some files into the project directory). 
  - Nuget and other dependencies...
  - Several enhancements for ASP.NET Core 1.1, including *Enable Docker Support*.
- In future, general features will not be added to .NET Framework or Core Separately, they will be added to .NET Standard instead. .NET Standard 2.0 is in preview.

## Mobile Development
- Cordova
- Ionic
- Xamarin 

## Streamlined cloud development
- Microservices 
- Docker
- DevOps
- .NET Core and Linux


## JavaScript

### Richer IntelliSense
1. Open a JavaScript file in VS2015 and VS2017 and type $ in a function to see how the Intellisense is different.
2. Type $.Ajax and see how the function signature is displayed.
3. Hit f12 over $.Ajax to see the *TypeScript Declaration .d.ts* file.

### Supports ES6+ syntax
- Import statements, classes, new keyword, etc. are all supported and detected.
- JSDOC comments are supported.
- Syntax highlighting for HTML inside JavaScript files, for Angular, React, etc. 

### Debug JavaScript Code in Chrome
- Demo
- Read more: https://blogs.msdn.microsoft.com/webdev/2016/11/21/client-side-debugging-of-asp-net-projects-in-google-chrome/


## Resources
- VisualStudio2017_ProductLaunchPoster.pdf poster
- Unified Platform and Tooling.png image
- [What�s new for developers in Visual Studio 2017](https://channel9.msdn.com/Events/Visual-Studio/Visual-Studio-2017-Launch/WEB-100) on Channel 9
